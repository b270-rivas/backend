db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: [ "Philippines", "US"]
	},

	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: [ "Philippines", "Ecuador"]
	},	

	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: [ "US", "China"]
	},	

	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: [ "Philippines", "India"]
	},	
]);

// MongoDB AGGREGATION

	// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data. 

// Using the Aggregate Method ($match)

	// Then $match is used to pass the documents that meet the spcified condition(s) to the next pipiline stage/aggregation process. 

		/*Syntax:
			{ $match: { field: value }}
		*/

		/*Syntax:
			{ $group: { _id: "value", fieldResult: "valueResult"} }
			Using the $match and $group along with aggregation will find for products that are on sale and will group the total amout of stock for suppliers found.
		*/ 

		/*Syntax:
			db.collectionName.aggregate([
				{ $match: {fieldA: valueA} }
				{ $group: {_id: "$fieldB"}, {result: {operatioin}} }
			]);

			_id is constant
		*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } }}
]);


// Field Projection and Aggregation ($project)

	// The $project can be used when agreegating data to include(1) or exclude(0) fields from the returned results.

		/*Syntax:
			{ $project: {field: 1/0} }
		*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } }},
	{ $project: {_id: 0} }
]);

// Sorting Aggregated Results

	// The $sort can be used to change the order of the aggregated results.
	// Providing a value of -1 will sort the aggregated result in a reverse order.

		/*Syntax:
			{ $sort: {field: 1/-1} }
		*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } }},
	{ $sort: {_id: -1} }
]);

// Aggregating Results Based on Array Fields

	// The $unwind deconstructs an array field from a collection/field with an array value to output a result for each element.

		/*Syntax:
			{ $unwind: "field" };
		*/

db.fruits.aggregate([
	{ $unwind: "$origin"}
]);

db.fruits.aggregate([
	{ $unwind : "$origin" },
	{ $group: {_id: "$origin", kinds: {$sum:1}} }
]);