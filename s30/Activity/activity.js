// $count operator - fruitsOnSale
db.fruits.aggregate([
	{ $match: {onSale: true} },
	{ $count: "fruitsOnSale"}
]);

// $count operator - enoughStock
db.fruits.aggregate([
	{ $match: {stock: {$gte: 20}} },
	{ $count: "enoughStock"}
]);

// $avg operator
db.fruits.aggregate([
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", avg_price: {$avg: "$price"}} }
]);

// $max operator
db.fruits.aggregate([
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", max_price: {$max: "$price"}} }
]);

// $min operator
db.fruits.aggregate([
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", min_price: {$min: "$price"}} }
]);