

let username;
let password;
let role;


function getUsername(){
    let username = prompt("Enter your username:");

    if(username === ""){
        alert("Username should not be empty.");
    } else if(username == null){
        alert("Username should not be empty.");
    }
}
getUsername();



function getPassword(){
    let password = prompt("Enter your password:");

    if(password === ""){
        alert("Password should not be empty.");
    } else if(password == null){
        alert("Password should not be empty.");
    }
}
getPassword();




function getRole() {
    let role = prompt("Enter your role:");

    if(role === "admin"){
        alert("Welcome back to the class portal, admin!");
    } else if(role === "teacher"){
        alert("Thank you for logging in, teacher!");
    } else if(role === "rookie"){
        alert("Welcome to the class portal, student!");
    } else {
        alert("Role out of range.");
    }
}
getRole();


function getAverage(num1, num2, num3, num4){
    let average = (num1 + num2 + num3 + num4) / 4;

    if (average <= 74) {
        console.log ("Hello, student, your average is: " + (Math.round(average)) + ". " + "The letter equivalent is F.");
    }

    else if (average >= 75 && average <= 79) {
        console.log ("Hello, student, your average is: " + (Math.round(average)) + ". " + "The letter equivalent is D");
    }

    else if(average >= 80 && average <= 84){
        console.log ("Hello, student, your average is: " + (Math.round(average)) + ". " + "The letter equivalent is C");
    }

    else if(average >= 85 && average <= 89){
        console.log ("Hello, student, your average is: " + (Math.round(average)) + ". " + "The letter equivalent is B");
    }

    else if(average >= 90 && average <= 95){
        console.log ("Hello, student, your average is: " + (Math.round(average)) + ". " + "The letter equivalent is A");
    }

    else if(average >= 96){
        console.log("Hello, student, your average is: " + (Math.round(average)) + ". " + "The letter equivalent is A+");
    }
}
getAverage();