const express = require('express');

const mongoose = require('mongoose');

const app = express();

const port = 3000;

// mongoose.connect - connects app to MongoDB database
mongoose.connect("mongodb+srv://admin:admin123@zuitt.sknrqtl.mongodb.net/b270-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true, //- prevents any current and future errors while connecting to MongoDB
		useUnifiedTopology : true //- connects to MongoDB even if the required url is updated. 
	}
);

// Notifies if the connection succeeded or failed.
const db = mongoose.connection;

//  If a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console as well as in the terminal
// "connection error" is the message that will display if this happens
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console 'We are connected to the database'
db.once("open", () => console.log("We are connected to the database"));

// Mongoose Schema
// Schemas determines the structure of documents to be written in the database.
const taskSchema = new mongoose.Schema({
	// Define the fields with corresponding data type
	name: String, // "name" field requires a String data type for the value
	status: { //"status" field requires String data type
		type: String,
		default: "pending" //allows users to leave this blank with a default value of "pending"
	}
});

// Mongoose Models
// Models use Schema and act as a middleman from the server to the database Server > Schema (blueprint) > Database > Collection
// "Task" variable can now be used to run commands for interacting with our database; the naming convention for mongoose models follows the MVC format
							/*the first parameter of the model() indicates the collection to store the data that will be created
							  ↓*/
const Task = mongoose.model("Task", taskSchema);
									/*↑
									the second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection*/

// Creation of routes
app.use(express.json()); //allow app to read json data
app.use(express.urlencoded({extended:true})); // allow app to read data from forms


// Business Logic - the way to write the code

// Create a new task

app.post('/tasks', (req, res) => {

	// Check for duplicate
	Task.findOne({name : req.body.name}).then((result, err) => {

		// If duplicate, return an error/notice:
		if (result != null && result.name === req.body.name){
			return res.send("Duplicate task found.")

		// If none, create new object and add to database:
		} else {
			let newTask = new Task({
				name: req.body.name
			})
			// Save the object in the collection
			newTask.save().then((savedTask, error) => {

				// Error handling best practice
				// try-catch-finally can also be used
				if (error){
					return console.error(error)
				} else {
					return res.status(201).send("New task created.")
				}
			})
		}
	})
});

// Getting all tasks

app.get('/get-tasks', (req, res) => {
	Task.find().then((result, err) => {
		if (err){
			return console.error(err)
		} else {
			return res.status(200).send(result)
		}
	})
});




// ACTIVITY CODEBASE

//1. Create a User schema.

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

//2. Create a User model.
const User = mongoose.model("User", userSchema);

//3. Create a POST route that will access the "/signup" route that will create a user.
app.post('/signup', (req, res) => {

	User.findOne({username: req.body.username}).then((result, err) => {

		if (result != null && result.username === req.body.username){
			return res.send("Duplicate user found.")

		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
			newUser.save().then((savedUser, error) => {
				if (error){
					return console.error(error)
				} else {
					return res.status(201).send("New user registered.")
				}
			})
		}
	})
});


app.listen(port, () => console.log(`Server running at ${port}`));