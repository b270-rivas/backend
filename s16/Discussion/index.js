// Arithmetic Operators

let x = 1397;
let	y = 7831;

let sum = x + y;
console.log("Result of additional operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = y / x;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


// Assignment Operators
console.log("Assignment Operators");

	// Basic Assignment Operator (=)
	let assignmentNumber = 8;
	console.log("Result of basic assignment operator: " + assignmentNumber);

	// Addition Assignment Operator (+=)
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Subtraction Assignment Operator (-=)
	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	// Multiplication Assignment Operator (*=)
	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	// Quotient Assignment Operator (+=)
	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators
console.log("Multiple Operators");

	// mdas
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operator: " + mdas);
	// answer = 0.6000000000000001

	/*
		The operations were done in the following:
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4
	*/

	// pemdas
	let pemdas = 1 + ( 2 - 3 ) * ( 4 / 5 );
	console.log("Result of pemdas operator: " + pemdas);
	// answer = 0.19999999999999996

	/*
		The operations were done in the following:
		1. 4 / 5 = 0.8
		2. 2 - 3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + - 0.8 = 0.2
	*/

// Type Coercion
console.log("Type Coercion");

	let numA = "10";
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	let noCoercion = numC + numD;
	console.log(noCoercion);
	console.log(typeof noCoercion);

	// The result is a number
	// The boolean (true) is also associated with the value of 1.
	let numE = true + 1;
	console.log(numE);

	// The boolean (falase) is also associated with the value of 0.
	let numF = false + 1;
	console.log(numF);

// Comparison Operators
console.log("Comparison Operators");

	let juan = "juan"

	// Equality Operator (==)
	/*
		- Checks whether the operands are equal/have the same value.
		- Returns a boolean value.
	*/
	console.log("Equality Operator")
	console.log(1 == 1); // true
	console.log(1 == 2); // false
	console.log(1 == "1"); // true
	console.log(0 == false); // true
	console.log("juan" == "juan"); // true
	console.log("juan" == juan); // true

	// Inequality Operator (!=)
	/*
	*/
	console.log("Inequality Operator")
	console.log(1 != 1); // false
	console.log(1 != 2); // true
	console.log(1 != "1"); // false
	console.log(0 != false); // false
	console.log("juan" != "juan"); // false
	console.log("juan" != juan); // false

	// Strict Equality Operator
	/*
		- Also compares the data types of the 2 values being compared.
	*/
	console.log("Strict Equality Operator")
	console.log(1 === 1); // true
	console.log(1 === 2); // false
	console.log(1 === "1"); // false
	console.log(0 === false); // false
	console.log("juan" === "juan"); // true
	console.log("juan" === juan); // true	

	// Strict Inequality Operator (!=)
	/*
	*/
	console.log("Inequality Operator")
	console.log(1 !== 1); // false
	console.log(1 !== 2); // true
	console.log(1 !== "1"); // true
	console.log(0 !== false); // true
	console.log("juan" !== "juan"); // false
	console.log("juan" !== juan); // false

// Relational Operators
console.log("Relational Operator");
let a = 50;
let b = 65;

	// Greater than Operators ( > )
	console.log("GreaterThan Operator");
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	// Less than Operators ( < )
	console.log("LessThan Operator");
	let isLessThan = a < b;
	console.log(isLessThan);

	// GreaterThanOrEqual Operators ( >= )
	console.log("GreaterThanOrEqual Operator");
	let isGreaterThanOrEqual = a >= b;
	console.log(isGreaterThanOrEqual);

	// LessThanOrEqual Operators ( <= )
	console.log("LessThanOrEqual Operator");
	let isLessThanOrEqual = a <= b;
	console.log(isLessThanOrEqual);

// Logical Operators
console.log("Logical Operator");

	// Logical && (AND) Operator - returns true if all operands are true

	let isLegalAge = true;
	let isRegistered = false;

	let allRequirements = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirements);

	// Logical || (OR) Operator - returns true if at least one operand is true
	let someRequirements = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirements);

	// Logical ! (NOT) Operator - returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " + someRequirementsNotMet);

// Increment and Decrement
console.log("Increment and Decrement");

let z = 1;

	// Pre-increment (++)
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	// Post-increment
	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);

	// Pre-decrement
	let decrement = --z;

	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);