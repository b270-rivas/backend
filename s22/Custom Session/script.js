// Activity 1
let socialMedia = ["Facebook", "Twitter", "Instagram", "Tiktok"];

let stringArray = socialMedia.toString();
console.log(stringArray);

// Activity 2
let avengers = ["Ironman", "Captain America", "Thor"];
avengers.splice(0, 0, "Black Panther");
avengers.splice(2, 0, "Black Widow");
console.log(avengers);

// Activity 3
let friends = ["Angel", "Anne", "Cheryl", "Nieva"];
friends.splice(3, 3, "Esme", "Aisa");
console.log(friends);