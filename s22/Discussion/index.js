// console.log("Array Methods")

/*MUTATOR METHODS
	- mutate or change an array after they are created.
	- manipulate the original array performing various tasks such as adding and removing elements.
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

console.log("Current fruits array:");
console.log(fruits);

// push - adds element at the end of an array AND returns the new array
/*Syntax:
	arrayName.push(elementToBeAdded);
*/

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method");
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

// unshift - adds element at the beginning of an array AND returns the new array
/*Syntax:
	arrayName.shift(element)
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift - removes the first element of an array AND returns the element
/*Syntax:
	let variableName = arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);

console.log("Mutated array from shift method");
console.log(fruits);


// pop - removes the last element of an array AND returns the element
/*Syntax:
	let variableName = arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);

console.log("Mutated array from pop method");
console.log(fruits);

//splice
/*Syntax:
	let variableName = arrayName.pop(startingIndex, deleteCount, elementsToBeAdded);
*/

let fruitsSplice = fruits.splice(1, 2, "Lime", "Cherry");
console.log(fruitsSplice);

console.log("Mutated array from splice method");
console.log(fruits);

// sort - rearranges the array elements in alphanumeric order
/*Syntax:
	arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse - reverses the current order of the array elements
/*Syntax:
	arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

/*NON-MUTATOR METHODS
	- are methods that do not modify or change an array after they're created.
	- do not manipulate the original array
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

/*indexOf
	- returns the index number of the first matching element found in an array.
	- search process will start from the first element proceeding to the last element.
	- if no match is found, it will return -1.
*/
/*Syntax:
	let variableName = arrayName.indexOf(searchElement);
*/

let firstIndex = countries.indexOf("PH", 2);
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);

/*lastIndexOf
	- returns the index number of the last matching element found in the array
	- search process will be done from the last element proceeding to the first element.
*/
/*Syntax:
	let variableName = arrayName.lastIndexOf(arrayElement);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

// slice - portions/slices elements from an array and returns a new array
/*Syntax:
	let variableName = arrayName.slice(startingIndex);
*/

let slicedArrayA = countries.slice(2);
console.log("Result of lastIndexOf method: ");
console.log(slicedArrayA);


let slicedArrayB = countries.slice(2, 5);
console.log("Result of lastIndexOf method: ");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result of lastIndexOf method: ");
console.log(slicedArrayC);

// toString - returns an array as a string separated by commas
/*Syntax:
	let variableName = arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result of toString method: ");
console.log(stringArray);

// concat - combines array and returns the combined array
/*Syntax:
	let variableName = arrayName.concat(addElement);
*/

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe bootstrap"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result of concat method: ");
console.log(tasks);

console.log("Result of concat method: ");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log("Result of concat method: ");
console.log(combinedTasks);

// join - returns an array with a separator ( , - /)

let users = ["John", "Jane", "Joe", "Joshua"];
console.log(users.join());
console.log(users.join(" "))

// ITERATION METHODS

/*forEach
	- similary to "for" loop that iterates on each array element
	- for each item in the array, the anonymous function passed in the forEach() will be run
	- the anonymous function is able to receive the current item being iterated or looped over by assigning a parameter
*/
/*Syntax:
	arrayName.forEach(function(argument){
		statement/code block
	})
*/

let filteredTasks = [];

allTasks.forEach(function(argument){
	if(argument.length > 10) {
		filteredTasks.push(argument);
	}
})
console.log("Result of filtered method: ");
console.log(filteredTasks);

// map
/*Syntax:
	let variableName = arrayName.map(function(argument){
		statement/code block
	})
*/
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(argument){
	return argument * argument;
})
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);

// forEach cannot return a value
let numberForEach = numbers.forEach(function(argument){
	return argument * argument;
})
console.log(numberForEach); //undefined


/*every
	- checks if all elements in an array meets the condition
	- returns a boolean value
*/
/*Syntax:
	let variableName = arrayName.every(function(argument){
		statement/code block
	})
*/

let allValid = numbers.every(function(argument){
	return (argument < 3);
})
console.log("Result of every method: ");
console.log(allValid);

/*some
	- check if at least one element in an array meets the condition
	- returns a boolean value
*/
/*Syntax:
	let variableName = arrayName.some(function(argument){
		statement/code block
	})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Result of some method: ");
console.log(someValid);

/*filter
	- returns a new array that contains the elements which meet the condition
	- returns an empty array if no elements were found
*/
/*Syntax:
	let variableName = arrayName.filter(function(argument){
		statement/code block
	})
*/

let filteredValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Result of filter method: ");
console.log(filteredValid);

/*includes
	- checks if element is included in the array
	- returns a boolean value
*/
/*Syntax:
	let variableName = arrayName.includes(elementToFindInArray);
*/

let products = ["Mouse", "Keyboard", "Monitor", "CPU" ];


let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

/*reduce
	- evaluates elements from left to right and returns/reduces the array into a single value
*/
/*Syntax:

*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("currentValue: " + x);
	console.log("accumulator: " + y);

	return x + y;
})
console.log("Result of reduce method: " + reducedArray)

let list = ["Hello", "Again", "World"];

let reducedString = list.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("currentValue: " + x);
	console.log("accumulator: " + y);

	return x + " " + y;
})
console.log("Result of reduce method: " + reducedString)