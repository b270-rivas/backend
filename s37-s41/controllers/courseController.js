const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
module.exports.addCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
	});
	return newCourse.save()
		// Course creation is successful
		.then(course => {
			console.log(course);
			res.send(true);
		})
		// Course creation failed
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
};

// Retrieve all courses
module.exports.getAllCourses = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {
		return Course.find({}).then(result => res.send(result));
	} else {
		return res.send({message: "Not authorized"});
	}
};

// Retrieve all active courses
module.exports.getAllActive = (req, res) => {
	return Course.find({isActive: true}).then(result => res.send(result));
}

// Retrieve a specific course
module.exports.getCourse = (req, res) => {
	console.log(req.params.courseId);

	return Course.findById(req.params.courseId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(result)
	})
};

// Update a course
module.exports.updateCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		};
		return Course.findByIdAndUpdate(req.params.courseId, updateCourse)
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
		console.log(error);
		return res.send(false);
		})
	} else {
		return res.send(false);
	}
};

// Archive a course
module.exports.archiveCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {
		let updateStatus = {
			isActive: false,
		};
		return Course.findByIdAndUpdate(req.params.courseId, updateStatus)
		.then((result) => {
			console.log(true);
			res.send(true);
		})
		.catch((error) => {
			console.log(error);
			return res.send(false);
		});
	} else {
		return res.send(false);
	}
};