const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
router.post('/', auth.verify, courseController.addCourse);

// Route for retrieveing all coures
router.get('/all', auth.verify, courseController.getAllCourses);

// Route for retrieveing all ACTIVE courses
router.get('/', courseController.getAllActive);

// Route for retrieving a specific course
router.get('/:courseId', courseController.getCourse);

// Route for updating a course
router.put('/:courseId', auth.verify, courseController.updateCourse);

// Route for archiving a course
router.patch('/:courseId/archive', auth.verify, courseController.archiveCourse);

module.exports = router;