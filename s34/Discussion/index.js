const express = require("express");

// This creates an express application and stores this in a constant called app.
// This is our server.
const app = express();

const port = 3000;

// Middlewares - a request handler that has access to the application's request-response cycle.
app.use(express.json());

app.use(express.urlencoded({extended:true})); // Allows app to read data from forms

// Routes
// Express has methods corresponding to each HTTP method

// app.get route expects to receive the GET request at the base URI "http://localhost:3000/"
app.get("/", (req, res) => {

	// res.send uses the Express JS module's method to send a response back to the client.
	res.send("Hello World!")
});

// This route
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});


let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} is successfully registered.`)
	} else {
		res.send(`Please input BOTH username and password.`)
	}
});

// ACTIVITY CODEBASE


//1. Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home", (req, res) => {

	// res.send uses the Express JS module's method to send a response back to the client.
	res.send("Welcome to the home page!")
});


//3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.get("/users", (req, res) => {
	res.send(users);
});

//5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-user", (req, res) => {
  let message;

  for (let i = 0; i < users.length; i++) {
    if (users[i].username === req.body.username) {
      users.splice(i, 1);
      message = `User ${req.body.username} has been deleted.`;
      break;
    } else {
      message = `User ${req.body.username} does not exist.`;
    }
  }

  res.send(message);
});


app.listen(port, () => console.log(`Server is now running at ${port}`));
