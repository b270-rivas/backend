//3. Insert a single room.
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities."
})

//4. Insert multiple rooms.
db.rooms.insertMany([

	{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation.",
		roomsAvailable: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen-sized bed perfect for a simple getaway.",
		roomsAvailable: 15,
		isAvailable: false
	}
])

//5. Use the find method to search for the double room.
db.rooms.find({name: "double"})

//6. Use the update method to update the queen room and set available rooms to 0.
db.rooms.updateOne(
	{name: "queen"},
	{
		$set: {
			roomsAvailable: 0,
		}
	}
)

//7. Use the delete method to delete all rooms that have 0 availability.
db.rooms.deleteOne({roomsAvailable: 0});