// alert("Hello Batch 270!");

// This is a statement (an instruction).

console.log("Hello again!");
console.log("Hi, Batch 270!");

/*
- There are two types of comments:
1. Single-line denoted by two slashes (CTRL+ /)
2. Multi-line denoted by a slash and asterisk (CTRL + SHIFT + /)
*/


// [SECTION] Variables
/*
- It is used to contain data.
*/

// Declaring Variables
// Syntax: let/constant variableName;

/*
- Trying to print out a value of a variable that has not been declared will return an error of "underfined"
- Variables must be  declared first with value before they can be used.
*/
let	myVariable;
console.log(myVariable);

let	greeting = "Hello";
console.log(greeting);

// Declaring and Initializing
/*
	// Initializing variables - the instance when a variable is given its initial value.
*/
// Syntax: let/constant variableName = value;
// let keyword - is used if we want to reassign values to our variable.

let	productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice)

const interest = 3.539
// interest = 2.5;
console.log(interest);

let friend = "Kate";
console.log(friend);

// we cannot re-declare variables wihint its scope
let friend1 = "Jay"
console.log(friend1)


// [SECTION] Data Types
/*
	String - series of characters that create a word, phrase, sentence or anything related to creating a text.
	Strings can be writing using either a single or double quote.
*/

let country = 'Philippine';
let province = 'Batangas';
console.log(country)
console.log(province);

// Concatenating strings

let fullAddress = province + ', ' + country;
console.log(fullAddress)

console.log('I live in the ' + country);

// "\n" - new line
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = 'John\'s employees went home early.';
console.log(message);

// Numbers
// Integers/Whole Numbers
let headCount= 26;
console.log(headCount);

// Decimal Numbers = blue font
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Boolean = true or false
let isMarried = false;
let inGoodConduct = true;
console.log("isMarried:" + isMarried);

// Arrays - store multiple data types. recommended to use similar data types.
// Syntax: let/const arrayName = [valueA, valueB, etc]
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details)

// Objects
/*
		Syntax:
		let/const objectName = {
			propertyA: valueA,
			propertyB: valueB,
		}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["09162328334", "09618504405"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}
console.log(person);

let subjects = {
	Math: "98.7",
	English: "92.1",
	Science: "90.2",
	History: "94.6",
}
console.log(subjects)

// null
let spouse = null;
console.log(spouse);

// undefined
let gender
console.log(gender)