// 4. Initialize/add the following trainer object properties.
let trainer = {
	name: "Ash Ketchup",
	age: 18,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},

	// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);

// 6. Access the trainer object properties using dot and square bracket notation.
console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// 7. Invoke/call the trainer talk object method.
console.log("Result of talk method:");
trainer.talk();

// 8. Create a constructor for creating a pokemon with the following properties.
function myPokemon(name, level) {

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.faint = function(){
		console.log(this.name + " has fainted.");
	}
	this.tackle = function(target) {

		let newHealth = target.health - this.attack;
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + " health is now reduced to " + newHealth);

		if (newHealth <= 0){
			target.faint();
		}
		target.health = newHealth
		console.log(target);
	};
}

let pikachu = new myPokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new myPokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new myPokemon("MewTwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);