// Activity 1
let cellphone = {
	name: "Samsung Galaxy S22+",
	storage: "128GB",
	OS: "Android",
	camera: {
		rear: "50.0 MP + 10.0 MP + 12.0 MP",
		front: "10.0 MP"
	}
};
console.log(cellphone);

// Activity 2
cellphone.resolution = "2340 x 1080 (FHD+)"
console.log(cellphone);

cellphone["memory"] = "8GB";
console.log(cellphone);

// Activity 3
let person = {
	firstName: "Darel",
	lastName: "Rivas",
	age: 28,
	talk: function(){
		console.log(`Hello! My name is ${this.firstName} ${this.lastName}. I am ${this.age} years old.`)
	}
}
person.talk();