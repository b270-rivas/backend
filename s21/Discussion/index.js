// console.log("Hello World!")

// Arrays are a list of data

let studentNumberA = "2023-1923";
let studentNumberB = "2023-1924";
let studentNumberC = "2023-1925";
let studentNumberD = "2023-1926";
let studentNumberE = "2023-1927";

let studentNumbers = ["2023-1923", "2023-1924", "2023-1925", "2023-1926", "2023-1927"]
console.log(studentNumbers);

// Common exampleof arrays
let grades = [71, 100, 85, 90];
console.log(grades);

let computerBrands = ["Acer", "Lenovo", "Dell", "Asus", "Apple", "Huawei"];
console.log(computerBrands);

// Possible use of an array but is not recommended
let mixedArr = [12, "Asus", null, undefined,{}];
console.log(mixedArr);

// Alternative way to write arrays
let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake bootstrap"
];
console.log(myTasks);

// Creating an array wtih values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Nairobi";
let city4 = "Rio";

let cities = [city1, city2, city3, city4];
console.log(cities);

// .length Property

console.log(myTasks.length);
console.log(cities.lenght);

let blankArr = [];
console.log(blankArr.length);

let fullName = "Darel Rivas";
console.log(fullName.length);

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

cities.length--;
console.log(cities);

// .length will not work with a string
/*fullName.length = fullName.length-1;
console.log(fullName);

fullName.length = fullName.length--;
console.log(fullName);*/

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);
theBeatles.length++;
console.log(theBeatles);

// Reading from arrays
console.log(grades[0]);

console.log(computerBrands[3]);

let lakersLegends = ["Kobe", "Lebron", "Shaq", "Magic", "Kareem"];

console.log(lakersLegends[3]); //Magic
console.log(lakersLegends[1]); //Lebron
console.log(lakersLegends[2]); //Shaq

// Store array values inside another variable
let currentLakers = lakersLegends[1];
console.log(currentLakers);

// Reassigning array values using index
console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Davis";
console.log("Array after reassignment");
console.log(lakersLegends);

// Accessing the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]); //Kukoc

console.log(bullsLegends[lastElementIndex]-1); //NaN