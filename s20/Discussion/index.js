console.log("Hello World!")

// "While" Loop
/*Syntax:
 	while(expression/condition){
		statement
 	}
*/

let count = 5;

while(count !== 0){
	console.log("While: " + count);
	count--;
}

// "Do-While" Loop
/*Syntax:
	do{
		statement/code block
	} while while(expression/condition)
*/

let number = Number(prompt("Give me a number."));

do {
	console.log("Do While: " + number);
	number += 1;
} while (number < 10);

// "For" Loop
/*Syntax:
	for (initialization; expression/condition; finalExpression){
		statement
	}
*/

for (let count = 0; count <= 20; count++){
	console.log(count);
} //Output 0-20

for (let count = 50; count >= 0; count--){
	console.log(count);
} //Output 50-0

let myString = "alex"
console.log(myString.length); //Output 4
// .length counts the number of characters in a string

console.log(myString[0]); //Output 0 = a
console.log(myString[1]); //Output 1 = l
console.log(myString[2]); //Output 2 = e
console.log(myString[3]); //Output 3 = x
// 0,1,2,3 are called index number. Always starts at 0.

for (let i = 0; i < myString.length; i++){
	console.log(myString[i])
} //Output alex

let myName = "darel";

for (let i = 0; i < myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}


// Continue and Break Statements

for (let count = 0; count <= 20; count++){

	if (count % 2 === 0) {
		continue;
		// If the statement is met then it will continue to the next iteration of the loop and ignores all other statements.
	}
	console.log("Continue and Break: " + count);
	// If the current value of "count" no longer meets the if statement then it proceeds to the next statement (console.log).

	if (count > 10){
		break;
		// If the statement has been satisfied then it will terminate the loop.
	}
}


let name = "alexandro"

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration.");
		continue;
	}
	if (name[i] === "d"){
		b
	}
}