// Activity 1
let myName = "incomprehensibility";

for ( let i = 0; i < myName.length; i++) {

    if (
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "o" ||
        myName[i].toLowerCase() == "u"
    ) {
        console.log("O");

    } else {
        console.log(myName[i])
    }
}


// Activity 2
let computerNumber = Math.floor(Math.random() * 10) + 1;
let hint = 'Guess my number, 1-10!';
let userIsGuessing = true;

while(userIsGuessing) {

	let guess = prompt(`${hint} Keep guessing!`)

	userIsGuessing++;

	if (computerNumber == guess){
		alert("Correct")
		break;
	}
	else if (computerNumber > guess){
		alert("Too low");
	}
	else if (computerNumber < guess){
		alert("Too high")
	}
	else {
		alert("That is not a number.");
	}
}


// Activity 3
let password;
do {
     password = prompt("What is the secret password?");
} while (password !== "password123");

alert("You know the secret password. Welcome!");