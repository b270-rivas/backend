console.log("Hello World!");
// conosole.log("Hello Again!");
console.log("Goodbye")


// Code blocking - waiting for the one statement to finish before executing the next statement.
for(let i = 0; i <= 10; i++){
	console.log(i);
};

console.log("Hello again!");

// ASYNCHRONOUS means that we can proceed to execute other statement while time-consuming code is running in the background.

// Getting all posts
// Fetch API allows us



fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status));

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));
// A fetch returns a "promise" which is an object that represents the eventual completion or failure of an asynchronous function and its resulting valud.
/*A "promise" can be in one of 3 possible states:
	pending - initial state, neither fulfilled nor rejected
	fulfilled - operation was completed
	rejected - operation failed
*/


fetch("https://jsonplaceholder.typicode.com/posts")
// Using "json()" converts the data retrieved to JS Object
.then(response => response.json())
.then(response => console.log(response));
/*.then(response => {
	response.forEach(post => console.log(post.title))
})*/

// "async" and "await" keywords are another approach that can be used to achieve asynchronous code.


async function fetchData(){

	// waits for the fetch to complete then stores the value in the result variable
	let result = await(fetch("https://jsonplaceholder.typicode.com/posts"));
	console.log(result);


	let json = await result.json();
	console.log(json);

}
fetchData();


// REST API
// GET - Getting a specific post
// ":id" is wildcard where we can put any value
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response => response.json())
.then(response => console.log(response));

// POST - Creating a post
/*Syntax:
	fetch("URL", {options})
	.then()
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userID: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));



// PUT - Updating a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Update Post",
		body: "Hello Again!",
		userID: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));


// PATCH - Updating a post using the PATCH method
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Patched Title Post"
	})
})
.then(response => response.json())
.then(response => console.log(response));


// DELETE - Delete a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(response => console.log(response));