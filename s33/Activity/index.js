const url = "https://jsonplaceholder.typicode.com/todos";

//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch(url)
.then(response => response.json())
//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
.then(data => {
	const titles = data.map(item => item.title);
	console.dir(titles);
});


//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch(`${url}/1/`)
.then(response => response.json())
.then(response => console.dir(response));

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch(`${url}/1/`)
.then(response => response.json())
.then(data => {
	const title = data.title;
	const status = data.completed ? "true" : "false";
	console.log(`The item "${title}" on the list has a status ${status}.`);
});


//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch(url, {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "New Post",
		body: "Created To-Do List",
		userID: 1
	})
})
.then(response => response.json())
.then(response => console.dir(response));

//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch(`${url}/1/`, {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},

	//9. Update a to do list item by changing the data structure.
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to-do list with a different data structure.",
		id: 1,
		status: "Pending",
		title: "Updated To-Do List",
		userID: 1
	})
})
.then(response => response.json())
.then(response => console.dir(response));

//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch(`${url}/1/`, {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},

	//11. Update a to do list item by changing the status to complete and add a date when the status was changed.
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then(response => response.json())
.then(response => console.dir(response));

//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch(`${url}/1/`, {
	method: "DELETE"
})
.then(response => response.json());