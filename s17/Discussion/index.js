/*Syntax:
	function functionName() {
	code block (statement)
	}

	function keyword - used to define a JS function
	functionName - iis the function's name. Functions are named to be used to called later in the code.
	function block {} - the statements which comprise the body of the function. This is where the code to be executed is written.
*/

	function printName() {
		console.log("Hi! My name is John.")
	};

// Function Invocation
	printName();

// Semicolons are used to separate executable JS statements or codes.

// Function Declaration - can be hoisted as long as the function has been declared.

	declaredFunction();

	function declaredFunction() {
		console.log("Hello World from declaredFunction()");
	};

// Function Expressions - can also be stored in a variable. Is anonymous function assigned to the variableFunction.

/*Syntax
	let variableName = function() {
	code block (statement)
	}
*/

	let n = 30; // this is how we initialize a variable
	let variableFunction = function () {
		console.log("Hello again Batch270!")
	};
	variableFunction();

	let funcExpression = function funcName() {
		console.log("Hello from the other side.")
	};
	funcExpression();

// You can reassign a declared functions and function expressions to a new anonymous functions.

	declaredFunction = function() {
		console.log("Updated declaredFunction")
	};
	declaredFunction();

	funcExpression = function() {
		console.log("Updated funcExpression")
	};
	funcExpression();

	const constantFunc = function() {
		console.log("Initialized with const")
	};
	constantFunc();
	declaredFunction();

/*
	const constantFunc = function() {
		console.log("Cannot be re-assigned - returns an error")
	};
	constantFunc();
*/

// Function Scoping

/*
	Scope is the accessibility (visibility) of variables within our program.

	JS variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/
	// Local Scope
	{
		let localVar = "Armando Perez";
		console.log(localVar);
	}

	// Global Scope
	let globalVar = "Mr. World";
	console.log(globalVar);

	// Function Scope
	function showNames() {
		// function scoped variables
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}
	showNames();

	// Nested Function
	// We can create another function inside a function. This is called a nested function. The nested function, being inside myNewFunction will have access to the variable name, as they are within the same scope/code block.
	function myNewFunction(){
		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
			console.log(nestedName);
		}
		nestedFunction();
	}
	myNewFunction();


	// Function and Global Scope Variables
	let globalName = "Alexandro"

	function myNewFunction2(){
		let nameInside = "Renz";

		console.log(globalName);
	}

	myNewFunction2();

	// Alert Variable
	/*Syntax
		alert()
	*/

	alert("Hello World!")

	function sampleAlert(){
		alert("Hello User!")
	}
	sampleAlert();

	console.log("I will only log in the console when the alert is dismissed.");

	// Prompt
	/*Syntax
		prompt("dialogInString");
	*/

	let samplePrompt = prompt("Enter your name");
	console.log("Hello," + samplePrompt);
	console.log(typeof samplePrompt);

	let sampleNullPromot = prompt("Don't enter anything");
	console.log(sampleNullPromot)

	function printWelcome(){
		let firstName = prompt("Enter your first name.");
		let lastName = prompt ("Enter your last name.");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}

	printWelcome();

	// Function Naming Conventions
	/*
	Function names should be descriptive of the task it will perform. It usually contains a verb.
	*/

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	function get(){
		let name = "Jamie";
		console.log(name);
	}
	get();

