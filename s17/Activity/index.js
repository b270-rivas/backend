/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personalInfo(){
		let fullName = prompt("Enter your full name.");
		let age = prompt("Enter your age.");
		let country = prompt("Enter your country of origin.");

		console.log("Full name: " + fullName);
		console.log("Age: " + age);
		console.log("Country: " + country);
	}
	personalInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteBand() {
		let bandA = "Paramore";
		let bandB = "Sleeping with Sirens";
		let bandC = "Beach Bunny";
		let bandD = "Chloe Moriondo";
		let bandE = "Rihanna";
		console.log(bandA + ', ' + bandB + ', ' + bandC + ', ' + bandD + ', ' + bandE);
	}
	favoriteBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovie(){
		let movieA = "The Glory";
		let ratingA = "83%";
		console.log(movieA);
		console.log("Rating" + " - " + ratingA);

		let movieB = "AOT";
		let ratingB = "95%";
		console.log(movieB);
		console.log("Rating" + " - " + ratingB);

		let movieC = "John Wick: Chapter 4";
		let ratingC = "94%";
		console.log(movieC);
		console.log("Rating" + " - " + ratingC);

		let movieD = "Old Guard";
		let ratingD = "80%";
		console.log(movieD);
		console.log("Rating" + " - " + ratingD);

		let movieE = "Gonjiam: Haunted Asylum";
		let ratingE = "91%";
		console.log(movieE);
		console.log("Rating" + " - " + ratingE);
	}
	favoriteMovie();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers(myFriends);
// let printFriends(myFriends);


	function printUsers(){

		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};
	printUsers();