// Import the http module using the require directive.
const http = require("http");

// Create a variable port and assign it with the value of 4000.
const port = 4000

// Create a server using the createServer() method that will listen in to the port provided.
const server = http.createServer((request, response) => {

	// Inside the createServer, input the codes:
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hello World!")
})

server.listen(port);

// Console log a message in the terminal  when the server is successfully running.
console.log(`Server is now accessible at localhost: ${port}.`);
