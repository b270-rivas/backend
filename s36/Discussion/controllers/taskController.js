// Controllers contain the functions and business logic of our Express application
// All the operations that it can do will be placed in this file
const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

module.exports.createTask = (requestBody) => {
	// Create a task object based on the model Task
	let newTask = new Task({
		// Sets the ame property with the value received from the client/postman
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error){
			console.log(error);
			return false
		} else {
			return task;
		}
	})
};

module.exports.deleteTask = (taskId) => {
	
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		
		if (err) {
			console.log(err);
			return false
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	
	return Task.findById(taskId).then((result, err) => {

		if (err) {
			console.log(err);
			return false
		}

		// Result of the "findById" will be stored in the "result" parameter
		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false
			} else {
				return updatedTask
			}
		})
	})
}

// ACTIVITY CODEBASE

//2. Controller function for retrieving a specific task
module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((findTask, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return findTask;
        }
    });
};

//2. Controller function for updating the status of a task to "complete".
module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {

		if (error) {
			console.log(error);
			return false
		}

		result.status = newStatus;

		return result.save().then((updatedStatus, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false
			} else {
				return updatedStatus
			}
		})
	})
}