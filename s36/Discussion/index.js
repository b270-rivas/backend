// Setup the dependencies
const express = require('express');

const mongoose = require('mongoose');

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoutes = require("./routes/taskRoutes")

// Server setup
const app = express();
const port = 3001;

//Connect to MongoDB database
mongoose.connect("mongodb+srv://admin:admin123@zuitt.sknrqtl.mongodb.net/b270-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true, //- prevents any current and future errors while connecting to MongoDB
		useUnifiedTopology : true //- connects to MongoDB even if the required url is updated. 
	}
);

// Notifies if the connection succeeded or failed.
const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We are connected to the database"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoutes);

// Server listening
app.listen(port, () => console.log(`Server running at ${port}`));