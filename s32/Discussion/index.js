let http = require("http");

const port = 4000;

// HTTP method of incoming request can be accessed via "method" property in request parameter
const server = http.createServer ((request, response) => {

	// Requests the "/items" path and "GETS" information
	// "GET" means that we will be retrieving/reading information
	if(request.url == "/items" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/pain"});

		// Ends the response process
		response.end("Data retrieved from the database.")
	}

	if(request.url == "/items" && request.method == "POST") {

		response.writeHead(200, {"Content-Type": "text/pain"});
		response.end("Data to be sent from the database.")
	}

});

server.listen(port);

console.log(`Server is live at port ${port}`);