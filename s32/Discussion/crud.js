let http = require("http");
let port = 4000;

// Mock Database

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Taylor",
		"email": "tswift@mail.com"
	}
];

let server = http.createServer((request, response) => {

	// check if the URL and method matches the required condition
	if (request.url == "/users" && request.method == "GET") {

		// content-type is set to applicaion/json to work with objects and less texts.
		response.writeHead(200, {"Content-Type": "application/json"});

		// the frontend and backend applications communicate through strings. we have to convert the JSON data type from the server into stringified JSON when it is sent to the frontend application
		response.write(JSON.stringify(directory));
		response.end();
	}


	if (request.url == '/users' && request.method == "POST") {

		// created a requestBody variable to an empty string
		let requestBody = '';
		// the requestBody will go through data stream; a sequence of data
		// data is received from the client and is process in the "data" stream
		request.on('data', function(data){

			// data step reads the "data" stream and processes it as the request body
			requestBody += data;
		});

		// end step runs after the request has been completely sent.
		request.on('end', function(){
			console.log(typeof requestBody);

			// converts a stringified JSON to JSON format (object)
			requestBody = JSON.parse(requestBody);
		let newUser = {
			"name": requestBody.name,
			"email": requestBody.email
		};


		// adding the JSON format to the directory/mock database
		directory.push(newUser);
		console.log(newUser);

		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(newUser));
		response.end();
		});
	}

});

server.listen(port);

console.log(`Server is running at localhost:${port}`);


