// If the url is http://localhost:4000/, send a response Welcome to Booking System
// If the url is http://localhost:4000/profile, send a response Welcome to your profile!
// If the url is http://localhost:4000/courses, send a response Here’s our courses available
// If the url is http://localhost:4000/addCourse, send a response Add a course to our resources
// If the url is http://localhost:4000/updateCourse, send a response Update a course to our resources
// If the url is http://localhost:4000/archiveCourses, send a response Archive courses to our resources


let http = require("http");
let port = 4000;

let server = http.createServer((request, response) => {

	if (request.url == "/" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to Booking System");

	} else if (request.url == '/profile' && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to your profile!");

	} else if (request.url == '/courses' && request.method == "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Here’s our courses available:");

	} else if (request.url == '/addCourse' && request.method == "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Add a course to our resources.");

	} else if (request.url == '/updateCourse' && request.method == "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update a course to our resources.");

	} else if (request.url == '/archiveCourses' && request.method == "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Archive courses to our resources");
		
	} else {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Page not found.");
	}

});

server.listen(port);

console.log(`Server is running at localhost:${port}`);